from pyro.infer import MCMC, NUTS
from sklearn.base import BaseEstimator

from .circuit import CircuitModel


class MCMCSampler(BaseEstimator):
    def __init__(self, num_samples=1000, warmup_steps=0):
        self.num_samples = num_samples
        self.warmup_steps = warmup_steps

    def fit(self, X: CircuitModel, y=None, init_params=None):
        # TODO clear parameter store
        mcmc = MCMC(
            kernel=NUTS(X.conditioned_model),
            num_samples=self.num_samples,
            warmup_steps=self.warmup_steps,
            initial_params=init_params
        )
        mcmc.run()
        self.mcmc_ = mcmc
        self.samples_ = mcmc.get_samples()
        return self
