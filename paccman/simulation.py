from typing import Optional

import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import pyro.distributions as dist
import torch
from sklearn.base import BaseEstimator
from tqdm import trange

from .circuit import CircuitModel


class CircuitSimulator(BaseEstimator):
    """Class that simulates neural circuit to obtain the steady-state responses.

    The circuit is simulated according to:

    .. math:: \tau\frac{dZ}{dt} = -Z + F((S\odot W)Z+I)

    Parameters
    ----------
    max_iter
        Maximum number of iterations of the simulation.

    tau
        Value of :math:`\tau` in ms

    dt
        Time difference in ms for numerical integration.

    random_state
        Determines random number generation for sampling from circuit priors. Use
        an int to make the randomness deterministic.

    verbose
        Verbosity mode.

    Attributes
    ----------
    S_ : torch.tensor
        Sign matrix used for simulation.

    W_ : torch.tensor
        Weight matrix used for simulation.

    Z_ : torch.tensor
        Latent response at the end of simulation.

    Zt_ : torch.tensor
        All latent responses during simulation.

    I_ : torch.tensor
        External input to the circuit used for simulation.

    Examples
    --------
    >>> import torch
    ...
    ... from paccman.circuit import CircuitModel
    ... from paccman.simulation import CircuitSimulator
    >>> circuit = CircuitModel()
    ... circuit.add_neuron(0)
    ... circuit.add_neuron(1)
    ... circuit.add_synapse(0, 1, sign=-1)
    <paccman.circuit.CircuitModel at 0x16163c81048>
    >>> I = torch.tensor([[1, 2], [0, 0]])
    >>> simulator = CircuitSimulator(random_state=42).fit(circuit, I)
    >>> simulator.S_
    tensor([[ 0.,  0.],
            [-1.,  0.]])
    >>> simulator.W_
    tensor([[0.0000, 0.0000],
      [1.6620, 0.0000]])
    >>> simulator.Z_
    tensor([[ 1.0000,  2.0000],
            [-1.6620, -3.3239]])
    """

    def __init__(self, max_iter: int = 10000, tau: float = 5., dt: float = 1., random_state: Optional[int] = None,
                 verbose: bool = False):
        self.max_iter = max_iter
        self.tau = tau
        self.dt = dt
        self.random_state = random_state
        self.verbose = verbose

    @staticmethod
    def _get_S(g: nx.DiGraph):
        S = torch.zeros(len(g), len(g))
        for u, v, synapse in g.edges(data='synapse'):
            S[v, u] = synapse.sign.sample() if isinstance(synapse.sign, dist.Bernoulli) else synapse.sign
        return S

    @staticmethod
    def _get_W(g: nx.DiGraph):
        W = torch.zeros(len(g), len(g))
        for u, v, synapse in g.edges(data='synapse'):
            W[v, u] = synapse.prior_dist.mean
        return W

    @staticmethod
    def _F(X: torch.tensor, g: nx.DiGraph):
        return torch.stack([neuron(X[i]) for i, neuron in g.nodes(data='neuron')])

    def fit(self, X: CircuitModel, y: torch.tensor):
        """Simulate circuit to obtain steady-state responses.

        Parameters
        ----------
        X
            Circuit model

        y : torch.tensor of shape (n_neurons, n_stimuli)
            External input to the circuit

        Returns
        -------
        self
            Simulated circuit simulator.
        """
        state = torch.get_rng_state()
        if isinstance(self.random_state, int):
            torch.manual_seed(self.random_state)

        g = nx.convert_node_labels_to_integers(X._circuit)
        I = y
        S = self._get_S(g)
        W = self._get_W(g)
        Weff = S * W

        Z = torch.zeros(I.shape)
        Zt = [Z]

        range_ = trange if self.verbose else range
        for _ in range_(self.max_iter):
            Z = Zt[-1]
            Zt.append(Z + (-Z + self._F(Weff @ Z + I, g)) * self.dt / self.tau)

        if isinstance(self.random_state, int):
            torch.set_rng_state(state)

        self.S_ = S
        self.W_ = W
        self.Z_ = Zt[-1]
        self.Zt_ = torch.stack(Zt)
        self.I_ = I

        return self

    def plot(self):
        if hasattr(self, 'Zt_'):
            plt.plot(self.Zt_.reshape(len(self.Zt_), np.prod(self.I_.shape)))
