"""
Various circuit element classes
"""

from typing import Union, Optional, Callable
from dataclasses import dataclass  # requires Python 3.7 or above
from numbers import Number

import numpy as np
import torch
import pyro
import pyro.distributions as dist


GAIN_PREFIX = "gain_"
OFFSET_PREFIX = "offset_"
XAXIS_PREFIX = "xaxis_"
DATA_PREFIX = "data_"
SIGN_PREFIX = "sign_"
FLOAT_TYPE = torch.float32
EPS = 1e-8


class _NonlinMixin:

    # TODO varying non-linearity parameters?

    shared_nonlin = None
    offset = None
    xaxis = None
    gain = None
    # vary_nonlin = True

    @property
    def nonlin_name(self):
        if self.shared_nonlin is None:
            return self.name
        else:
            return self.shared_nonlin

    def sample_gain(self):
        """
        Sample nonlinearity gain
        """
        return pyro.sample(f"{GAIN_PREFIX}{self.nonlin_name}", self.gain)

    def sample_offset(self):
        """
        Sample nonlinearity offset
        """
        return pyro.sample(f"{OFFSET_PREFIX}{self.nonlin_name}", self.offset)

    def sample_xaxis(self):
        """
        Sample xaxis offset
        """
        return pyro.sample(f"{XAXIS_PREFIX}{self.nonlin_name}", self.xaxis)

    def __call__(self, tensor):
        """
        Applies nonlinearity
        """

        if self.nonlin is None:
            return tensor
        if self.gain is not None:
            gain = self.sample_gain()
            tensor = gain * tensor
        if self.xaxis is not None:
            xaxis = self.sample_xaxis()
            tensor = tensor - xaxis
        if self.offset is None:
            return self.nonlin(tensor)
        else:
            offset = self.sample_offset()
            return self.nonlin(tensor - offset) + self.nonlin(offset)


@dataclass
class Neuron(_NonlinMixin):
    """
    Neuron class that holds the following attributes
    """
    name: str
    # intrinsic nonlinearity
    nonlin: Optional[Callable] = None
    # the gain prior if nonlinearity is given - this specifies the prior
    gain: Optional[dist.LogNormal] = dist.LogNormal(0, 1)
    # offset to fit if nonlinearity is given - this specifies the prior
    offset: Optional[dist.Normal] = dist.Normal(0, 1)
    # offset along x-axis only for non-linearity
    xaxis: Optional[dist.Normal] = None
    # if a latent variable or not
    vary: bool = True
    # distribution type {'normal', 'lognormal', 'poisson'}
    dist_type: str = 'normal'
    # scale prior
    scale_prior: float = 1.0
    # mean prior
    mean_prior: float = 0.0
    # str shared nonlin name
    shared_nonlin: Optional[str] = None

    def _dist(self, mean, scale):
        """
        Get distribution for neuron given mean and sample
        """
        if self.dist_type == 'normal':
            return dist.Normal(mean, scale)
        elif self.dist_type == 'lognormal':
            return dist.LogNormal(mean, scale)
        elif self.dist_type == 'poisson':
            return dist.Poisson(torch.exp(mean))
        else:
            raise ValueError(
                "`dist_type` must be {'normal', 'lognormal', 'poisson'}, "
                f"but is '{self.dist_type}'."
            )


@dataclass
class Synapse:
    """
    Synapse class that holds the following attributes
    """
    # name of presynaptic neuron
    presynaptic: str
    # name of postsynaptic neuron
    postsynaptic: str
    # prior distribution
    prior_dist: dist.LogNormal = dist.LogNormal(0, 1)
    # sign of synapse (defaults to bernoulli)
    sign: Union[float, dist.Bernoulli] = dist.Bernoulli(0.5)
    # whether to fix or vary parameter (i.e. fixed to mean of prior_dist)
    vary: bool = True
    # whether to fit latent or not
    fit_params: bool = True

    @property
    def name(self):
        return f"{self.presynaptic}->{self.postsynaptic}"

    def sample_sign(self):
        """
        Sample the sign for the synapse
        """
        if isinstance(self.sign, dist.Bernoulli):
            if self.fit_params:
                sign = pyro.sample(f"{SIGN_PREFIX}{self.name}", self.sign)
            else:
                # do not register sampling
                sign = self.sign.sample()
            if sign:
                return sign
            else:
                return sign - 1

        else:
            return self.sign

    def sample(self):
        """
        Sample the (signed) weight
        """
        s = self.sample_sign()

        if self.vary:
            if self.fit_params:
                w = pyro.sample(self.name, self.prior_dist)
            else:
                w = self.prior_dist.sample()
            return s * w

        else:
            return s * self.prior_dist.mean


@dataclass
class NeuronData(_NonlinMixin):
    """
    Dataclass that holds the following attributes
    """

    def __post_init__(self):
        if isinstance(self.data, np.ndarray):
            self.data = torch.tensor(self.data, dtype=FLOAT_TYPE)

        if self.data.ndim == 1:
            self.data = self.data[:, None]
        elif self.data.ndim > 2:
            raise ValueError(
                "The data for a `NeuronData` object must "
                f"be one- or two-dimensional, but `data` is {self.data.ndim}."
            )

        self.mask = ~torch.isnan(self.data)
        # is this the correct type for mathematical operations
        self.mask_float = self.mask.type(FLOAT_TYPE)
        # create nanless data
        data = self.data.clone()
        data[~self.mask] = 0
        self.nanless = data
        n = torch.sum(self.mask_float, axis=1)
        # mean
        self.mean = torch.sum(self.nanless, axis=1) / n
        # unbiased variance
        # nanvar
        if self.scale is None:
            # default variance of 1 if not enough samples - TODO change?
            var = torch.ones(n.shape, dtype=FLOAT_TYPE)
            var[n > 1] = torch.sum(
                ((self.nanless - self.mean[:, None]) ** 2) * self.mask_float,
                axis=1
            )[n > 1] / (n - 1.0)[n > 1]
            self.scale = torch.sqrt(var)
        else:
            if isinstance(self.scale, Number):
                self.scale = torch.ones(n.shape, dtype=FLOAT_TYPE) * self.scale
            elif isinstance(self.scale, np.ndarray):
                self.scale = torch.tensor(self.scale, dtype=FLOAT_TYPE)
            assert self.scale.shape == n.shape

    # name of parameter
    neuron_name: str
    # data tensor
    data: Union[torch.tensor, np.ndarray]
    # extrinsic nonlinearity
    nonlin: Optional[Callable] = None
    # the gain prior if nonlinearity is given - this specifies the prior
    gain: Optional[dist.Distribution] = None
    # offset to fit if nonlinearity is given - this specifies the prior
    offset: Optional[dist.Distribution] = dist.Normal(0, 1)
    # offset along x-axis only for non-linearity
    xaxis: Optional[dist.Normal] = None
    # population standard deviation (same length as data)
    scale: Optional[Union[torch.tensor, np.ndarray, float]] = None
    # str shared nonlin name
    shared_nonlin: Optional[str] = None
    # normalize
    normalize : Optional[bool] = True

    @property
    def name(self):
        return f"{DATA_PREFIX}{self.neuron_name}"

    def sample(self, mean):
        # TODO add neuron instance to check vary
        # takes the empirical variance
        mean = self(mean) # calling the nonlinearity
        # broadcast to normalize each observation
        data, mean = torch.broadcast_tensors(self.nanless, mean[:, None])
        # normalize mean predictions
        if self.normalize:
            mean = mean * (
                torch.sqrt(torch.sum(data ** 2 * self.mask_float, axis=0))[None]
                / (torch.sqrt(torch.sum(mean ** 2 * self.mask_float, axis=0)) + EPS)[None]
            )
        # uses the empirical std in the normal
        # TODO is this how to apply the mask for observations?
        with pyro.plate(f"{self.name}_observed", len(self.nanless)):            
            # with pyro.poutine.mask(mask=self.mask):
            s = pyro.sample(
                self.name, 
                dist.Normal(mean, self.scale[:, None]).to_event(),
                obs=self.nanless
            )

        return s

    def __getitem__(self, key):
        # if key is tuple then indexing happens along multiple axes
        if isinstance(key, tuple):
            scale_key = key[0]
        else:
            scale_key = key
        # return new NeuronData instance
        return type(self)(
            neuron_name=self.neuron_name,
            data=self.data[key],
            nonlin=self.nonlin,
            gain=self.gain,
            offset=self.offset,
            scale=self.scale[scale_key]
        )

    def __len__(self):
        return self.data.shape[0]

    # think about best iteration method
    # def __iter__(self):
    #     return iter(self.data)

    @property
    def size(self):
        return self.data.size

    @property
    def shape(self):
        return self.data.shape

    @property
    def ndim(self):
        return self.data.ndim
