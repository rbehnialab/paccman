"""
Circuit class
"""

from typing import Optional

import networkx as nx
import torch
import pyro

from .elements import Neuron, Synapse, NeuronData


# TODO
# - get mean effective weight matrix after fitting
# - get mean latent_X after fitting
# - plotting functionality
# - generateing data
# - sampling from circuit after fitting (return dictionary of latents)
# - slicing of circuit data for splitting dataset and cross-validation
# - same external nonlinearity
# - same internal nonlinearity
# - sharing of parameters between fitted models
class CircuitModel:
    """
    Circuit model class that implements `conditioned_model` method.

    Parameters
    ----------
    circuit : networkx.DiGraph
    """

    def __init__(self, circuit: Optional[nx.DiGraph] = None):
        if circuit is None:
            self._circuit = nx.DiGraph()
        else:
            assert isinstance(circuit, nx.DiGraph)
            self._circuit = circuit

        # Neuron instances are stored in the `neuron` key of the circuit node
        # Synapse instances are stored in the `synapse` key of edge attributes
        # Data instances are stored in the `data` key of the circuit node

        # sample size ones data has been added
        self._sample_size = None

    @property
    def circuit(self):
        return self._circuit

    @property
    def neurons(self):
        """
        Dictionary with `Neuron` instances for each neuron.

        The keys are the names of the neurons.
        """
        return {
            key: value.get('neuron', Neuron(key))
            for key, value in self.circuit.nodes.items()
        }

    @property
    def synapses(self):
        """
        Dictionary with `Synapse` instances for each existing synapse.

        The keys are a two-tuple of form (presynaptic, postsynaptic)
        """
        return {
            key: value.get('synapse', Synapse(key[0], key[1]))
            for key, value in self.circuit.edges.items()
        }

    @property
    def data(self):
        """
        Dictionary with `NeuronData` instances for each neuron.

        The keys are the names of the neurons.

        Neurons with no data are still present in dictionary but have
        None as their value.
        """
        return {
            key: value.get('neuron_data', None)
            for key, value in self.circuit.nodes.items()
        }

    @property
    def sample_size(self):
        if self._sample_size is None:
            # TODO messaging if size error
            for node_name, neuron_data in self.data.items():
                if neuron_data is not None:
                    self._sample_size = neuron_data.data.shape[0]
                    break
        return self._sample_size

    def add_neuron(self, name, **neuron_kwargs):
        """
        Adds node to nx.DiGraph (if not exists) and creates Neuron instance
        """
        neuron = Neuron(name, **neuron_kwargs)
        self.circuit.add_node(name, neuron=neuron)
        return self

    def add_synapse(self, presynaptic, postsynaptic, **synapse_kwargs):
        """
        Adds edge to nx.DiGraph (if not exists) and creates Synapse instance
        """
        assert postsynaptic in self.circuit.nodes, (
            f"Postsynaptic neuron `{postsynaptic}` not in circuit. "
            "Specify neuron first with `add_neuron` method."
        )
        assert presynaptic in self.circuit.nodes, (
            f"Presynaptic neuron `{presynaptic}` not in circuit. "
            "Specify neuron first with `add_neuron` method."
        )
        synapse = Synapse(presynaptic, postsynaptic, **synapse_kwargs)
        self.circuit.add_edge(presynaptic, postsynaptic, synapse=synapse)
        return self

    def add_data(self, neuron_name, data, **data_kwargs):
        """
        Add data to circuit model and create NeuronData instance.

        Also sets sample size, if it is the first data entered.

        All data within one circuit have to have the same length
        (i.e. same set of stimuli used.)
        """
        assert neuron_name in self.circuit.nodes, (
            f"Neuron `{neuron_name}` not in circuit. "
            "Specify neuron first with `add_neuron` method."
        )
        if self.sample_size is None:
            self._sample_size = data.shape[0]
        else:
            assert data.shape[0] == self.sample_size, (
                f"Data size of {data.shape[0]} does not match sample "
                f"size of {self.sample_size} for circuit."
            )
        neuron_data = NeuronData(neuron_name, data, **data_kwargs)
        self.circuit.nodes[neuron_name].update({'neuron_data': neuron_data})
        return self

    def inputs(self, neuron):
        """
        Return list of inputs to a neuron
        """
        return [pre for pre, post in self.circuit.in_edges(neuron)]

    def __len__(self):
        return (0 if self.sample_size is None else self.sample_size)

    def __getitem__(self, key):
        # copy digraph and slice NeuronData instances
        circuit = self.circuit.copy()
        for neuron_name, attrs in circuit.nodes.items():
            # pop NeuronData instance from node attributes
            neuron_data = attrs.pop('neuron_data', None)

            if neuron_data is not None:
                # slice NeuronData instance
                circuit.nodes[neuron_name]['neuron_data'] = neuron_data[key]
        return type(self)(circuit)

    # TODO correlation-only based model?

    def conditioned_model(self):
        """Model for pyro
        """

        neurons = self.neurons
        synapses = self.synapses
        data = self.data

        # t-1
        # latent variable "current" (used to compare to yt)
        ytminus1 = {}
        # latent response/release (used as inputs and compare to data)
        xtminus1 = {}
        for name, neuron in neurons.items():
            # get data instance
            neuron_data = data[name]
            # NOTES
            # sample from latents
            # y = "current" and x = "response/release"
            if neuron_data is None or neuron.vary:
                # uninformative prior
                y = pyro.sample(
                    name,
                    neuron._dist(
                        neuron.mean_prior * torch.ones((self.sample_size,)),
                        neuron.scale_prior
                    ).to_event()
                )
            else:
                # NOTES
                # neuron_data is not sampled if neuron.vary is False
                y = neuron._dist(
                    neuron_data.mean, neuron_data.scale
                ).sample()
            # apply nonlinearity
            x = neuron(y)
            # "current"
            ytminus1[name] = y
            # "voltage"/"release"
            xtminus1[name] = x
            # if vary and data available
            if neuron_data is not None and neuron.vary:
                neuron_data.sample(x)

        # t
        for name, neuron in neurons.items():
            inputs = self.inputs(name)

            # NOTES
            # don't do anything if no inputs exist
            # -> yt and yt-1 are already the same
            if inputs:
                # iterate over inputs and concatenate them
                for idx, input in enumerate(inputs):
                    # keeps pre, post order as for networkx
                    w_ij = synapses[(input, name)].sample()
                    # get latest latent value for input
                    x_j = xtminus1[input]

                    if idx == 0:
                        x = x_j[:, None]
                        w = w_ij[None]
                    else:
                        x = torch.cat([x, x_j[:, None]], axis=1)
                        w = torch.cat([w, w_ij[None]], axis=0)

                # normalize w
                # TODO option to normalize or not
                # TODO specify L1 or L2 normalization
                w = w / torch.sum(torch.abs(w))
                # normalize x
                # TODO specify L1 or F-norm
                x = x / torch.norm(x)
                # linear prediction
                y_pred = x @ w
                # ensure yt and yt-1 are similar (even if data does not exist)
                with pyro.plate(f"{name}_tobserved", len(ytminus1[name])):  
                    pyro.sample(
                        f"{name}_t",
                        neuron._dist(y_pred, neuron.scale_prior),
                        obs=ytminus1[name]
                    )
