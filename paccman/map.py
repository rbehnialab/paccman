import numpy as np
from pyro.infer import SVI, Trace_ELBO
from pyro.infer.autoguide import AutoDelta
from pyro.optim import Adam
from sklearn.base import BaseEstimator
from tqdm import trange

from .circuit import CircuitModel


class MAPEstimator(BaseEstimator):
    def __init__(self, max_iter=1000, learning_rate=1e-2, verbose=False):
        self.max_iter = max_iter
        self.learning_rate = learning_rate
        self.verbose = verbose

    def fit(self, X: CircuitModel, y=None):
        # TODO clear parameter store
        model = X.conditioned_model
        guide = AutoDelta(model)
        optimizer = Adam({'lr': self.learning_rate})
        loss = Trace_ELBO()
        guide()
        svi = SVI(model, guide, optimizer, loss)
        range_ = trange if self.verbose else range
        self.model_ = model
        self.guide_ = guide
        self.loss_curve_ = np.array(
            [svi.step() for _ in range_(self.max_iter)]
        )
        self.map_estimates_ = {
            key: value.detach()
            for key, value in guide.median().items()
        }
        return self
